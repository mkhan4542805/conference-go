from .models import Attendee
from common.json import ModelEncoder
from events.api_views import ConferenceListEncoder


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name", "company_name", "created", "conference"]
    encoders = {
        "conference": ConferenceListEncoder(),
    }
